<?php

/**
 * BombBomb API Wrapper for PHP
 *
 * PHP version 5
 *
 * LICENCE: This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category	API_Integration
 * @package		BombBomb
 * @author		Peter Walsh <peter@perceptiveweb.solutions>
 * @copyright	2015 Perceptive Web Solutions
 * @license		GPLv3 http://www.gnu.org/licenses/
 * @link		http://peterjwalsh.ca/projects/bombbomb
 * @see			http://bombbomb.com/api/
 */

/**
 * API method pseudo-enum 
 */
abstract class ApiMethod {
	
	const IS_VALID_LOGIN  				= 'IsValidLogin';
	const GET_VIDEOS      				= 'GetVideos';
	const ADD_VIDEO       				= 'AddVideo';
	const VIDEO_QUICKSEND 				= 'VideoQuickSend';
	const GET_LISTS       				= 'GetLists';
	const CREATE_LIST     				= 'CreateList';
	const DELETE_LIST     				= 'DeleteList';
	const GET_LIST_CONTACTS 			= 'GetListConstacts';
	const ADD_CONTACT     				= 'AddContact';
	const UPDATE_CONTACT 	 			= 'UpdateContact';
	const GET_CONTACT     				= 'GetContact';
	const ADD_EMAIL_TO_LIST 			= 'AddEmailToList';
	const REMOVE_EMAIL_FROM_LIST 		= 'RemoveEmailFromList';
	const GET_CONTACT_FIELDS 			= 'GetContactFields';
	const IMPORT_CSV_TO_LIST 			= 'importCsvToList'; //BombBomb-- Why does this one method start lowercased?
	const GET_LIST_PROCESSING_STATUS 	= 'GetListProcessingStatus';
	const GET_EMAILS 					= 'GetEmails';
	const GET_EMAIL_TAGS 				= 'GetEmailTags';
	const GET_ALL_EMAILS_BY_TAG 		= 'GetAllEmailsByTag';
	const SEND_EMAIL_TO_EMAIL_ADDRESS 	= 'SendEmailToEmailAddress';
	const SEND_CUSTOMER_VIDEO_EMAIL 	= 'SendCustomerVideoEmail';
	const EMAIL_TRACKING 				= 'emailTracking'; //another method that starts lowercased...
	const GET_DRIPS 					= 'GetDrips';
	const ADD_TO_DRIP 					= 'addToDrip'; // another
	const REMOVE_FROM_DRIP 				= 'removeFromDrip'; // another
}

/**
 * BombBomb API Wrapper
 * 
 * @example BombBomb::withApiKey('secret')->sendEmailToEmailAddress($emailId, 'user@example.com');
 * @example $mail = BombBomb::withEmailAndPassword($email, $pswd);
 *			$mailingLists = $mail->getLists();
 */
class BombBomb {
	
	private static $API_ENDPOINT = 'https://app.bombbomb.com/app/api/api.php';	
	
	private $data;
	private $curl;
	private $requestBody;
	
	private function __construct() {
		/* Constructor is declared private to prevent instantiation. Intead use one of 
 		 * the static helper methods (::withApiKey and ::withEmailAndPassword).
		 */
		 	
		$this->data        = array();
		$this->requestData = array();
		$this->curl        = $this->_curlInit();
	}	

	/**
	 * Instantiates a new BombBomb object and sets the post fields required for
	 * email and password authentication.
	 */
	public static function withEmailAndPassword($email, $password) {
		
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			error_log('BombBomb::new: Email address fails email validation');
			return null;
		} if (empty($password)) {
			error_log('BombBomb::new: Password can not be null or empty');
			return null;
		}

		$instance = new BombBomb();
		$instance->setPostParam('email', $email);
		$instance->setPostParam('pw', $password);
		return $instance;
	}
	
	/**
	 * Instantiates a new BombBomb object and sets the post fields required for
	 * API key authentication.
	 */
	public static function withApiKey($key) {
		
		if (empty($key)) {
			error_log('BombBomb::withApiKey: key can not be null or empty');
			return null;
		}
		
		$instance = new BombBomb();
		$instance->setPostParam('api_key', $key);
		return $instance;
	}
	
	// Private Functions
	
	/**
	 * Create & initialize our cURL object 
	 */
	private function _curlInit() {
		
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_URL, BombBomb::$API_ENDPOINT);
		
		return $curl;
	}
	
	private function setPostParam($key, $value) {
		
		$this->postData[$key] = $value;
	}
	
	private function getPostParam($key) {
		
		if(isset($this->postData[$key]))
			return $this->postData[$key];
	}
	
	/**
	 * Reset our internal representation of the request POST data
	 */
	private function clearPostParams() {
		
		foreach($this->postData as $key => $value) {
			// Make sure we're not unsetting our auth creds
			if ($key !== 'email' && $key !== 'pw' && $key !== 'api_key')
				 unset($this->postData[$key]);
		}
	}
	
	/**
	 * Perform API Request
	 */
	private function performRequest() {
		
		$postData = BombBomb::arrayToPost($this->postData);
		curl_setopt($this->curl, CURLOPT_POSTFIELDS, $postData);
		$response = curl_exec($this->curl);
		$this->clearPostParams();
		
		return $this->parseResponse($response);
	}
	
	/**
	 * Parse the JSON response. If an error occurs log it in the system 
	 * error log & return false
	 */
	private function parseResponse($response) {
		
		$decodedResponse = json_decode($response);
		if (!$decodedResponse || $decodedResponse->status == 'failure') {
			if (!$decodedResponse)
				error_log('BombBomb::parseResponse: Could not parse JSON');
			if ($decodedResponse->status == 'failure')
				error_log('BombBomb::parseResponse: API returned failure status: ' . $response);
			return false;
		}
			
		return $decodedResponse;
	}
	
	/**
	 * Set POST params required for the create and update contact endpoints
	 *
	 * @email required
	 * @listIds required
	 */
	private function setContactFields($email, $listIds,
									  $firstname = null, 
									  $lastname = null, 
									  $phone = null, 
									  $addr1 = null, 
									  $addr2 = null, 
									  $city = null, 
									  $state = null, 
									  $country = null, 
									  $postalCode = null, 
									  $company = null, 
									  $position = null, 
									  $comments = null) {
										  
		$this->setPostParam('eml', $email);
		$this->setPostParam('listlist', $listIds);
		if ($firstname)  { $this->setPostParam('firstname',      $firstname); }
		if ($lastname)   { $this->setPostParam('lastname',       $lastname); }
		if ($phone)      { $this->setPostParam('phone_number',   $phone); }
		if ($addr1)      { $this->setPostParam('address_line_1', $addr1); }
		if ($addr2)      { $this->setPostParam('address_line_2', $addr2); }
		if ($city)       { $this->setPostParam('city',           $city); }
		if ($state)      { $this->setPostParam('state',          $state); }
		if ($country) 	 { $this->setPostParam('country',        $country); }
		if ($postalCode) { $this->setPostParam('postal_code',    $postalCode); }
		if ($company)    { $this->setPostParam('company',        $company); }
		if ($position)   { $this->setPostParam('position',       $position); }
		if ($comments)   { $this->setPostParam('comments',       $comments); }
		if (is_array($customFields) && !empty($customFields)) {
			foreach($customFields as $key => $value) {
				$this->setPostParam($key, $value);
			}
		}
	}
	
	// Public functions
	
	public static function isValidLogin($email, $password) {
		/* The one API method that doesn't require authentication
		 * so we need to go through the entire process here in order
		 * to make this a static method
		 */
		 
		$curl     = curl_init();
		$postData = array(
			'method' => ApiMethod::IS_VALID_LOGIN,
			'email'  => $email,
			'pw'     => $password
		);
		
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_URL, BombBomb::$API_ENDPOINT);
		curl_setopt($curl, CURLOPT_POSTFIELDS, BombBomb::arrayToPost($postData));
		$result = curl_exec($curl);
		curl_close($curl);
		
		return json_decode($result);
	}
	
	/**
	 * Convert associative array to POST data suitable for storage
	 * in a HTTP request body
	 */
	public static function arrayToPost($array) {
		
		$post = '';
		
		foreach($array as $key => $value) {
			$post .= $key . '=' . urlencode($value) . '&';
		}
		rtrim($post, '&');

		return $post;
	}
	
	// API Methods
	
	/**
	 *
	 */
	public function getVideos() {
		
		$this->setPostParam('method', ApiMethod::GET_VIDEOS);
		$response = $this->performRequest();
		
		return $response->info ? $response->info : false;
	}
	
	/**
	 *
	 */
	public function addVideo($name, $videoFile, $desc = null) {
		
		$this->setPostParam('method', ApiMethod::ADD_VIDEO);
		$this->setPostParam('name', $name);
		//sending binary data needs some looking into
		//TODO: Research & complete
	}
	
	/**
	 *
	 */
	public function videoQuickSend($emailAddresses, $listId, $videoId, $subject, $body) {
		
		$this->setPostParam('method', ApiMethod::VIDEO_QUICKSEND);
		$this->setPostParam('email_addresses', $emailAddresses);
		$this->setPostParam('list_id', $listId);
		$this->setPostParam('video_id', $videoId);
		$this->setPostParam('subject', $subject);
		$this->setPostParam('mobile_message', $body);
		$response = $this->performRequest();
		
		return $response->info ? $response->info : false;		
	}
	
	/**
	 *
	 */
	public function getLists() {
		
		$this->setPostParam('method', ApiMethod::GET_LISTS);
		$response = $this->performRequest();
		
		return $response->info ? $response->info : false;
	}
	
	/**
	 *
	 */
	public function createList($name) {
		
		$this->setPostParam('method', ApiMethod::CREATE_LISTS);
		$this->setPostParam('name', $name);
		$response = $this->performRequest();
		
		return $response->info ? $response->info : false;
	}
	
	/**
	 *
	 */
	public function deleteList($id) {
		
		$this->setPostParam('method', ApiMethod::DELETE_LIST);
		$this->setPostParam('list_id', $id);
		$response = $this->performRequest();
		
		return $response->info ? $response->info : false;
	} 
	
	/**
	 *
	 */
	public function getListContacts($listId) {
		
		$this->setPostParam('method', ApiMethod::GET_LIST_CONTACTS);
		$this->setPostParam('list_id', $listId);
		$response = $this->performRequest();
		
		return $response->info ? $response->info : false;
	}
	
	/**
	 *
	 */
	public function addContact($email, $listIds, //required params
							   $firstname = null, 
							   $lastname = null, 
							   $phone = null, 
							   $addr1 = null, 
							   $addr2 = null, 
							   $city = null, 
							   $state = null, 
							   $country = null, 
							   $postalCode = null, 
							   $company = null, 
							   $position = null, 
							   $comments = null, 
							   $customFields = null) {
		
		$this->setPostParam('method', ApiMethod::ADD_CONTACT);
		$this->setContactFields($email, $listIds, $firstname, $lastname, 
								$phone, $addr1, $addr2, $city, $state, 
								$country, $postalCode, $company, $position, 
								$comments, $customFields);
		$response = $this->performRequest();
		
		return $response->info ? $response->info : false;
	}
	
	/**
	 *
	 */
	public function updateContact($email, $listIds,
							   	  $firstname = null, 
							   	  $lastname = null, 
							      $phone = null, 
							      $addr1 = null, 
							      $addr2 = null, 
							      $city = null, 
							      $state = null, 
							      $country = null, 
							      $postalCode = null, 
							      $company = null, 
							      $position = null, 
							      $comments = null, 
							      $customFields = null) {
		
		$this->setPostParam('method', ApiMethod::UPDATE_CONTACT);
		$this->setContactFields($email, $listIds, $firstname, $lastname, 
								$phone, $addr1, $addr2, $city, $state, 
								$country, $postalCode, $company, $position, 
								$comments, $customFields);
		$response = $this->performRequest();
		
		return $response->info ? $response->info : false;
	}
}