<?php
	require('../src/BombBomb.class.php');
	
	class BombBombTest extends PHPUnit_Framework_TestCase {
		
		protected $mailer;
		protected $email = 'krystel@teamleads.com';
		protected $pswd  = 'a272a14841079f2af29fd8300fd90ec5';
		
		protected function setUp() {
			
			$this->mailer = BombBomb::withEmailAndPassword($this->email, $this->pswd);
			$this->assertTrue($this->mailer !== false);
		}
		
		public function testGetLists() {
			
			$lists = $this->mailer->getLists();
			$this->assertTrue(is_array($lists));
		}
		
		public function testGetVideos() {
			
			$videos = $this->mailer->getVideos();
			//echo print_r($videos);
			$this->assertTrue(is_array($videos));
		}
		
		public function testIsValidLogin() {
			
			$result = BombBomb::isValidLogin($this->email, $this->pswd)->status;
			$areCredsValid = $result === 'success' ? true : false;
			$this->assertTrue($areCredsValid);
		}
		
		public function testVideoQuickSend() {
			
			$videos = $this->mailer->getVideos();
			$lists  = $this->mailer->getLists();
			
			$result = $this->mailer->videoQuickSend(
				'xorgate@live.com',
				$lists[0]->id,
				$videos[0]->id,
				'test message',
				'test body'
			);
			print_r($result);
		} 
	}